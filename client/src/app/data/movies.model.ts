export module MoviesDto {
    export interface ViewingWindow {
        startDate: string;
        wayToWatch: string;
        endDate: string;
    }

    export interface CardImage {
        url: string;
        h: number;
        w: number;
    }

    export interface Director {
        name: string;
    }

    export interface KeyArtImage {
        url: string;
        h: number;
        w: number;
    }

    export interface Cast {
        name: string;
    }

    export interface Alternative {
        quality: string;
        url: string;
    }

    export interface Video {
        title: string;
        alternatives: Alternative[];
        type: string;
        url: string;
    }

    export interface Movie {
        skyGoUrl: string;
        url: string;
        reviewAuthor: string;
        id: string;
        cert: string;
        viewingWindow: ViewingWindow;
        headline: string;
        cardImages: CardImage[];
        directors: Director[];
        sum: string;
        keyArtImages: KeyArtImage[];
        synopsis: string;
        body: string;
        cast: Cast[];
        skyGoId: string;
        year: string;
        duration: number;
        rating: number;
        class: string;
        videos: Video[];
        lastUpdated: string;
        genres: string[];
        quote: string;
    }

}

