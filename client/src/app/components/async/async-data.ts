import {
    Directive,
    Input,
    TemplateRef,
    ViewContainerRef,
    Output,
    EventEmitter,
    ComponentFactoryResolver,
    Component
  } from '@angular/core';
  import { Observable, Subscription } from 'rxjs';
  
  export class LoadedData {
    constructor(public data: any) {
    }
  }
  
  @Component({
    selector: 'async-spinner',
    template: '<app-spinner></app-spinner>',
  })
  export class AsyncSpinnerComponent {
  }
  
  @Component({
    selector: 'async-error',
    template: '<div class="alert alert-danger">{{error}}</div>',
    styles: ['.alert {margin: 10px 0;}']
  })
  export class AsyncErrorComponent {
    error: string;
  }
  
  @Directive({selector: '[asyncData]'})
  export class AsyncDataDirective {
    @Output() dataLoaded: EventEmitter<any> = new EventEmitter<any>();
    @Output() errorReceived: EventEmitter<string> = new EventEmitter<string>();
    @Output() finished: EventEmitter<void> = new EventEmitter<void>();
  
    private subscription: Subscription;
  
    constructor(private templateRef: TemplateRef<any>,
                private viewContainer: ViewContainerRef,
                private componentFactoryResolver: ComponentFactoryResolver) {
    }
  
    @Input()
    set asyncData(observable: Observable<any>) {
      if (this.subscription) {
        this.subscription.unsubscribe();
      }
      if (observable) {
        // Show spinner
        let factory = this.componentFactoryResolver.resolveComponentFactory(AsyncSpinnerComponent);
        this.viewContainer.createComponent(factory);
        this.subscription = observable.subscribe(data => {
          // Show data
          this.viewContainer.clear();
          this.viewContainer.createEmbeddedView(this.templateRef, new LoadedData(data));
          this.dataLoaded.emit(data);
        }, error => {
          // Show error
          this.viewContainer.clear();
          let factory = this.componentFactoryResolver.resolveComponentFactory(AsyncErrorComponent);
          let errorComponent = this.viewContainer.createComponent(factory);
          errorComponent.instance.error = error;
          this.errorReceived.emit(error);
        }, () => {
          this.finished.emit();
        })
      } else {
        console.log("No observable found in AsyncDataComponent");
      }
    }
  }
  