import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { MoviesService } from '../../services/movies.service';
import { MoviesDto } from '../../data/movies.model';

@Component({
  selector: 'app-movies',
  templateUrl: './movies.component.html',
  styleUrls: ['./movies.component.scss']
})
export class MoviesComponent implements OnInit {
  // Observable to load movies async
  movies$: Observable<Array<MoviesDto.Movie>>;

  constructor(private moviesService: MoviesService) { }

  ngOnInit() {
    // Pull movies from server on page load
    this.movies$ = this.moviesService.loadMovies();
  }

  selectMovie(movie) {
    this.moviesService.setSelectedMovie(movie);
  }
}
