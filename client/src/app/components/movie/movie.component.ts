import { ActivatedRoute, ParamMap, Router } from "@angular/router";
import { Observable } from 'rxjs/Observable';
import { Component } from "@angular/core";

import { MoviesService } from '../../services/movies.service';
import { MoviesDto } from '../../data/movies.model';

@Component({
    selector: 'app-movie',
    templateUrl: './movie.component.html',
    styleUrls: ['./movie.component.scss']
  })
  export class MovieComponent {
    movie: MoviesDto.Movie;

    constructor(private moviesService: MoviesService, private route: ActivatedRoute) {
      route.params.subscribe((params) => {
        // Actually I don't use the param anywhere for now but I like observables :)
        this.movie = this.moviesService.selectedMovie;
      });
    }
  }
