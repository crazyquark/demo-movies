import { Component } from "@angular/core";

@Component({
  selector: 'app-spinner',
  template: '<div class="spinner-container"><div class="spinner"></div></div>',
  styleUrls: ['spinner.component.scss'],
})
export class SpinnerComponent {

}
