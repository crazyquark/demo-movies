import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from "@angular/router";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './components/app.component';
import { MoviesComponent } from './components/movies/movies.component';
import { MovieComponent } from './components/movie/movie.component';
import { MoviesService } from './services/movies.service';
import { HttpClientModule } from '@angular/common/http';
import { SpinnerComponent } from './components/spinner/spinner.component';
import { AsyncDataDirective, AsyncErrorComponent, AsyncSpinnerComponent } from "./components/async/async-data";

// Will just go to /movies when you access /
let routes = [
  {
    path: 'movies',
    component: MoviesComponent
  },
  {
    path: 'movies/:id',
    component: MovieComponent
  },
  {
    path: '',
    redirectTo: '/movies',
    pathMatch: 'full'
  }
];

@NgModule({
  declarations: [
    AppComponent,
    AsyncDataDirective, 
    AsyncErrorComponent, 
    AsyncSpinnerComponent, 
    SpinnerComponent,
    MovieComponent,
    MoviesComponent
  ],
  entryComponents: [
    AsyncErrorComponent, AsyncSpinnerComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(routes),
  ],
  providers: [{provide: LocationStrategy, useClass: HashLocationStrategy}, MoviesService],
  bootstrap: [AppComponent]
})
export class AppModule { }
