import { Injectable } from "@angular/core";
import { MoviesDto } from "../data/movies.model";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs/Observable";
import 'rxjs/add/operator/map';

import { environment } from '../../environments/environment';

@Injectable()
export class MoviesService {

  movies: Array<MoviesDto.Movie>;
  selectedMovie: MoviesDto.Movie;

  constructor(private http: HttpClient) {
  }

  loadMovies(): Observable<Array<MoviesDto.Movie>> {
    return this.http.get(environment.apiUrl)
      .map(items => {
        let result = new Array<MoviesDto.Movie>();
        Array.prototype.push.apply(result, items);
        return result;
      });
  }

  setSelectedMovie(movie: MoviesDto.Movie) {
    this.selectedMovie = movie;
  }

  loadMovie(id: string): Observable<MoviesDto.Movie> {
    return this.http.get(environment.apiUrl + '/' + id).map(movie => <MoviesDto.Movie>movie);
  }
}