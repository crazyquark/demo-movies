package ro.cs.movies.cache;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import ro.cs.movies.data.BaseImage;
import ro.cs.movies.data.MovieInfo;

@Component
public class MoviesCache {
	private ZonedDateTime lastRefresh;
	private String dataUrl;
	
	private MovieInfo[] cached;
	
	// For fast access
	private Map<String, MovieInfo> moviesIndex = new HashMap<>();
	private Map<String, BaseImage> imagesCache = new HashMap<>();

	private static Logger logger = Logger.getLogger(MoviesCache.class);


	@Autowired
	public MoviesCache(@Value("${resource.dataUrl}") String dataUrl) {
		this.dataUrl = dataUrl;
		
		try {
			this.refreshMoviesResource();
		} catch (RestClientException e) {
			// If loading the data fails then I have no reason to be
			logger.fatal("Failed to load cache: " + e.getLocalizedMessage());
			System.exit(1);
		} catch (URISyntaxException e) {
			logger.fatal("Failed to load cache: " + e.getLocalizedMessage());
			System.exit(1);
		}
	}

	private void refreshMoviesResource() throws RestClientException, URISyntaxException {
		RestTemplate client = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		
		HttpEntity<MovieInfo[]> entity = new HttpEntity<MovieInfo[]>(headers);

		ResponseEntity<MovieInfo[]> response = client.exchange(new URI(this.dataUrl),HttpMethod.GET, entity, MovieInfo[].class);
		
		ZonedDateTime lastModified = null;
		try {
			String lastModifiedHttpDate = response.getHeaders().get("Last-Modified").get(0);
			lastModified = ZonedDateTime.parse(lastModifiedHttpDate, DateTimeFormatter.RFC_1123_DATE_TIME);
		} catch (Exception ex) {
			logger.warn("Should not happen: failed to parse HTTP Last-Modified, error: " + ex.getMessage());
			this.lastRefresh = null;
		}
		
		if (lastRefresh == null || lastModified == null || lastRefresh.compareTo(lastModified) != 0) {
			logger.info("Refreshing data source, hold on...");
			this.cached = response.getBody();	
			
			for (MovieInfo movie : this.cached) {
				this.moviesIndex.put(movie.getId(), movie);
				
				for (BaseImage image : movie.getCardImages()) {
					this.imagesCache.put(image.getUrl(), image);
				}
				for (BaseImage image : movie.getKeyArtImages()) {
					this.imagesCache.put(image.getUrl(), image);
				}
			}
			
			this.lastRefresh = lastModified;
			logger.info("Finished refreshing data");
		}
	}

	public MovieInfo[] getAllMovies() {
		try {
			this.refreshMoviesResource();
		} catch (RestClientException | URISyntaxException e) {
			logger.warn("Failed to refresh data: " + e.getLocalizedMessage());
		}
		
		return this.cached;
	}
	
	public byte[] getImage(String url) {
		BaseImage image = this.imagesCache.get(url);
		if (image == null) {
			return null;
		}
		
		File imageFile = image.loadImage();
		try {
			InputStream in = new FileInputStream(imageFile);
			ByteArrayOutputStream out = new ByteArrayOutputStream(1024);

			byte[] chunk = new byte[1024];
			int read = 0;
			do {
				try {
					read = in.read(chunk);
				} catch (IOException e) {
					in.close();
					return null;
				}
				if (read > 0) {
					out.write(chunk, 0, read);
				}
			} while (read > 0);
			
			in.close();
			
			return out.toByteArray();
		} catch (FileNotFoundException e1) {
			return null;
		} catch (IOException e) {
			return null;
		}
	}
	
	public MovieInfo getMovie(String id) {
		return this.moviesIndex.get(id);
	}
}
