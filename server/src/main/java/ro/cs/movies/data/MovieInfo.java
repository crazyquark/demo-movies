
package ro.cs.movies.data;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class MovieInfo implements Serializable
{
    private String url;
    private String skyGoUrl;
    private String reviewAuthor;
    private String id;
    private String cert;
    private ViewingWindow viewingWindow;
    private String headline;
    
    private List<CardImage> cardImages = null;
    
    private List<Director> directors = null;
    private String sum;
    
    private List<KeyArtImage> keyArtImages = null;
    
    private String synopsis;
    private String body;
    private List<Cast> cast = null;
    private String skyGoId;
    private String year;
    private int duration;
    private int rating;
    
    @JsonProperty("class")
    private String _class;
    
    private List<Video> videos = null;
    
    private String lastUpdated;
    private List<String> genres = null;
    private String quote;
    
    private final static long serialVersionUID = 2668962839943365687L;
}
