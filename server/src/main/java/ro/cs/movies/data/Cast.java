
package ro.cs.movies.data;

import java.io.Serializable;

import lombok.Data;

@Data
public class Cast implements Serializable
{
    private String name;

    private final static long serialVersionUID = 3210054514898757313L;
}
