
package ro.cs.movies.data;

import java.io.Serializable;
import java.util.List;

import lombok.Data;

@Data
public class Video implements Serializable
{

    private String title;
    private List<Alternative> alternatives = null;
    private String type;
    private String url;
    
    private final static long serialVersionUID = -5192482969921962135L;
}
