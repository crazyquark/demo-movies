package ro.cs.movies.data;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.function.Function;

import org.apache.log4j.Logger;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.ByteArrayHttpMessageConverter;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
public class BaseImage {
	@Getter(AccessLevel.NONE)
	@Setter(AccessLevel.NONE)
	private boolean isCached = false;
	
	@Getter(AccessLevel.NONE)
	@Setter(AccessLevel.NONE)
	private String sourceUrl;
	
	@Getter(AccessLevel.NONE)
	private String url;

	private int h;
	private int w;

	private static Logger logger = Logger.getLogger(BaseImage.class);

	public String getUrl() {
		if (this.sourceUrl != null) {
			// We rewrote this URL
			return this.url;
		}
		
		// We never read this URL, rewrite it
		this.sourceUrl = this.url; // save URL for fetching
		String path = null;
		
		try {
			// Extract relative path
			path = new URL(this.url).getPath();
			this.url = path;
		} catch (MalformedURLException e) {
			logger.warn("Should not happen, failed to parse URL: " + e.getLocalizedMessage());
			return this.url;
		}		
		
		return this.url;
	}

	private Void cacheImage(byte[] data) {
		if (!this.isCached && data != null) {
			// Happily cache the image;
			ClassLoader classLoader = getClass().getClassLoader();
			File file  = new File(classLoader.getResource(".").getFile() + this.url);
			try {
				File parent = file.getParentFile();
				if (!parent.exists() && !parent.mkdirs()) {
				    logger.warn("Couldn't create dir: " + parent);
				    return null;
				}
				
				if(file.createNewFile()) {
					FileOutputStream outFile = new FileOutputStream(file);
					outFile.write(data);
					outFile.close();
					this.isCached = true;
				}
			} catch (IOException e) {
				logger.warn("Failed to write file: " + e.getLocalizedMessage());
			}
		}
		
		this.isCached = true; // Never attempt to cache this file again
		
		return null;
	}
	
	protected void fetchImage(Function<byte[], Void> operation) {
		RestTemplate restTemplate = new RestTemplate();
		restTemplate.getMessageConverters().add(new ByteArrayHttpMessageConverter());

		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_OCTET_STREAM));

		HttpEntity<String> entity = new HttpEntity<String>(headers);

		try {
			ResponseEntity<byte[]> response = restTemplate.exchange(sourceUrl, HttpMethod.GET, entity, byte[].class);

			if (response.getStatusCode() == HttpStatus.OK) {
				operation.apply(response.getBody());
			} else {
				logger.warn(
						"Failed to retrieve image: " + sourceUrl + ", status was: " + response.getStatusCodeValue());
				operation.apply(null);
			}

		} catch (RestClientException e) {
			// TODO: this just pollutes the log
			// logger.warn("Failed to access URL:" + sourceUrl + ", error was:" +
			operation.apply(null);
		}
	}
	
	public File loadImage() {
		if (this.isCached) {
			return getLocalFile();
		} else {
			if (this.sourceUrl != null) {
				this.fetchImage(this::cacheImage);
				
				return getLocalFile();
			}
			
			// Zoinks!
			return null;
		}
	}

	private File getLocalFile() {
		ClassLoader classLoader = getClass().getClassLoader();
		return new File(classLoader.getResource(".").getFile() + this.url);
	}
}
