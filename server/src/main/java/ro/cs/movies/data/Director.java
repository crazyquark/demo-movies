
package ro.cs.movies.data;

import java.io.Serializable;

import lombok.Data;

@Data
public class Director implements Serializable
{
    private String name;

    private final static long serialVersionUID = -7524438271392377879L;
}
