
package ro.cs.movies.data;

import java.io.Serializable;

import lombok.Data;

@Data
public class Alternative implements Serializable
{

    private String quality;
    private String url;

    private final static long serialVersionUID = 8228823642334420789L;
}
