
package ro.cs.movies.data;

import java.io.Serializable;

import lombok.Data;

@Data
public class ViewingWindow implements Serializable
{

    private String startDate;
    private String wayToWatch;
    private String endDate;
    
    private final static long serialVersionUID = -4353797545741186797L;
}
