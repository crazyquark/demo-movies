package ro.cs.movies.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.HandlerMapping;

import ro.cs.movies.cache.MoviesCache;

@RestController
@RequestMapping("/images")
public class ImagesController {
	@Autowired
	private MoviesCache moviesCache;
	
	@RequestMapping(value = "/{path}/**", method = RequestMethod.GET)
	public ResponseEntity<byte[]> getImage(@PathVariable("path") String path, HttpServletRequest request) {
		String fullPath = (String) request.getAttribute(HandlerMapping.PATH_WITHIN_HANDLER_MAPPING_ATTRIBUTE);
		
		byte[] image = moviesCache.getImage(fullPath);
		
		if (image != null) {
			return new ResponseEntity<byte[]>(image, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
}
