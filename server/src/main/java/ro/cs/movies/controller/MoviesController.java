package ro.cs.movies.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ro.cs.movies.cache.MoviesCache;
import ro.cs.movies.data.MovieInfo;

@RestController
@RequestMapping("/api/movies")
public class MoviesController {	
	@Autowired
	private MoviesCache cache;
	
	@RequestMapping("")
	public MovieInfo[] getAllMovies() {
		return cache.getAllMovies();
	}
}
