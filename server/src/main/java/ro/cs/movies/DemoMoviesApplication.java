package ro.cs.movies;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoMoviesApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoMoviesApplication.class, args);
	}
}
