package config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = {"ro.cs.movies"})
/**
 * Spring config class
 * 
 * @author Cristian Sandu
 *
 */
public class AppConfig {

}
