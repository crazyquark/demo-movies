## Server ##
From the `server` folder:  

Use gradle or the wrapper by running:  
`./gradlew bootRun`  
to start the server (on Windows use gradelw.bat)

## Client ##
From the `client` folder:

You will need `@angular/cli` and `npm`(nodejs) to run the frontend. Once you have that installed, just run  
`npm start`

Info: webpack will proxy api calls to the backend to avoid CORS errors(see proxy.conf.json)
